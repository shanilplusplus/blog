const express =  require('express');
const bodyParser =  require('body-parser');
const app = express();
const methodOverride = require("method-override");
const expressSanitizer = require('express-sanitizer');
const request = require('request');
const mongoose = require('mongoose');
const port = process.env.PORT || 4679;
app.use(express.static(__dirname + '/public'));
app.use('/', express.static(__dirname + '/www'));
app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js'));
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist'));
app.use(bodyParser.urlencoded({extended: true}));
app.set('view engine', 'ejs');
app.use(methodOverride('_method'));
app.use(expressSanitizer());
 

 mongoose.connect('mongodb://shanil:3v9P4egNCmJS@ds139984.mlab.com:39984/blog-shanil');

 mongodb://shanil:3v9P4egNCmJS@ds139984.mlab.com:39984/blog-shanil

 var blogSchema = new mongoose.Schema({
	title : String,
	image: String,
	body: String,
	category: String, 
	author: String,
	authorImage: String,
	website: String,
	twitter: String,
	created: {type: Date, default: Date.now} 
});

var Blog = mongoose.model("Blog", blogSchema);

app.get('/', function(req,res){
	Blog.find({}, function(err, blogs){
		if(err){
			console.log('error!');
		} else {
			res.render('index', {blogs, blogs});
		}
	});
});

app.post('/', function(req,res){
	req.body.blog.body = req.sanitize(req.body.blog.body);
	Blog.create(req.body.blog, function(err, newBlog){
		if(err){
			res.render('new');
		} else{
			res.redirect('/');
		}
	});
});

app.get('/new', function(req, res){
	res.render('new');
})
app.get('/about', function(req,res){
	res.render('about');
});

app.get('/contact', function(req,res){
	res.render('contact');
});
app.post('/contact', function(req, res){
	// console.log(req.body);
	var name = req.body.name;
	res.render('contact', {
		page : 'contact ',
		name: name
	});	
});

app.get('/:id', function(req,res){
	Blog.findById(req.params.id, function(err, foundBlog){
		if(err){
			res.redirect('/');
		} else{
		res.render('show',  {blog: foundBlog});
		}
	});
});

app.get('/:id/edit', function(req,res){
	Blog.findById(req.params.id, function(err, foundBlog){
		if(err){
			res.redirect('/');
		} else {
			res.render('edit', {blog: foundBlog});
		}
	});
});

app.put('/:id', function(req,res){
	Blog.findByIdAndUpdate(req.params.id, req.body.blog, function(err, blogUpdate){
		if(err){
			res.redirect("/");
		} else {
			res.redirect('/' + req.params.id);
		}
	});
})

app.delete('/:id', function(req,res){
	Blog.findByIdAndRemove(req.params.id, function(err){
		if(err){
			res.redirect('/');
		} else{
			res.redirect('/');
		}
	}) 
})
app.get('*', function(req,res){
	res.render('error');
});

app.listen(port , function(){
	console.log('server is up');
});
